## Algebra e teoria elementar dos números

# Teoria
[x] - Unidade 1
    [ ] - Conceito de álgebra
[ ] - Unidade 2 - Propriedades algébricas
    [x] - Polinômios
    [x] - Produtos notáves
    [ ] - Fatoração
[ ] - Unidade 3
[ ] - Unidade 4

# Finalização

[ ] - Anotações esquematizadas dos conceitos principais da algebra utilizando árvores para demonstrar a aprendizagem.

# Prático (Solo)
[ ] - Fixação - Polinômios 